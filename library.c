#include "library.h"

#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

#include <ifaddrs.h>
#include <netdb.h>


#include <sys/ioctl.h>
#include <stdbool.h>
#include <netinet/ip6.h>


#define ICMP_V6_PROTOCOL_NO 58
#define USER_INPUT_INTERFACE_NAME_BUFFER_SIZE 32
#define USER_INPUT_FIELDS_BUFFER_SIZE 32

void find_interface_and_address_by_user_input_interface_name(char *p_interface_name, struct in6_addr *p_ip_v6_address) {
    struct ifaddrs *p_interface_list_head, *p_interface;
    char trimmed_string_address[INET6_ADDRSTRLEN];
    char entered_interface_name[USER_INPUT_INTERFACE_NAME_BUFFER_SIZE];
    _Bool is_match = false;

    while (!is_match) {
        memset(entered_interface_name, '\0', USER_INPUT_INTERFACE_NAME_BUFFER_SIZE);
        printf("Please enter the name of interface you wish to use: ");
        fgets(entered_interface_name, USER_INPUT_INTERFACE_NAME_BUFFER_SIZE, stdin);
        entered_interface_name[strlen(entered_interface_name) - 1] = '\0';

        //get available interfaces linked list
        getifaddrs(&p_interface_list_head);
        p_interface = p_interface_list_head;

        while (p_interface != NULL) {

            if (p_interface->ifa_addr->sa_family == AF_INET6
                && !strncmp(p_interface->ifa_name, entered_interface_name, strlen(p_interface->ifa_name))) {

                char string_address[INET6_ADDRSTRLEN];
                memset(string_address, '\0', INET6_ADDRSTRLEN);
                memset(trimmed_string_address, '\0', INET6_ADDRSTRLEN);

                getnameinfo(p_interface->ifa_addr, sizeof(struct sockaddr_in6), string_address,
                            INET6_ADDRSTRLEN, NULL, 0, NI_NUMERICHOST);

                for (int i = 0; i < INET6_ADDRSTRLEN; i++) {
                    if (string_address[i] == '%') { break; }
                    trimmed_string_address[i] = string_address[i];
                }

                is_match = inet_pton(AF_INET6, trimmed_string_address, p_ip_v6_address) == 1;
                strncpy(p_interface_name, p_interface->ifa_name, strlen(p_interface->ifa_name) );

                break;
            }

            p_interface = p_interface->ifa_next;
        }

        if (!is_match) {
            printf("Interface %s has not been found \n", entered_interface_name);
        } else {
            printf("Found interface %s with address: %s.\n", p_interface->ifa_name, trimmed_string_address);
        }

        freeifaddrs(p_interface_list_head);
    }
}

void set_ipv6_version_by_user_input(struct ip6_hdr *p_ip_v6_header) {
    char input_buffer[USER_INPUT_FIELDS_BUFFER_SIZE];
    memset(input_buffer, '\0', USER_INPUT_FIELDS_BUFFER_SIZE);

    printf("Please enter IP header version (default is 6)\n");
    fgets(input_buffer, USER_INPUT_FIELDS_BUFFER_SIZE, stdin);
    unsigned int version = 6;
    if (strlen(input_buffer) > 1) { version = (unsigned int) strtoul(input_buffer, NULL, 0); }
    if (version > 15) { version = 6; }

    p_ip_v6_header->ip6_ctlun.ip6_un1.ip6_un1_flow = htonl(version << 28);
}

void set_ipv6_payload_length_by_user_input(struct ip6_hdr *p_ip_v6_header) {
    char input_buffer[USER_INPUT_FIELDS_BUFFER_SIZE];
    memset(input_buffer, '\0', USER_INPUT_FIELDS_BUFFER_SIZE);

    printf("Please enter IP header payload size (default: automatically)\n");
    fgets(input_buffer, USER_INPUT_FIELDS_BUFFER_SIZE, stdin);
    unsigned short payload_length = 0;
    if (strlen(input_buffer) > 1) { payload_length = (unsigned short) strtoul(input_buffer, NULL, 0); }

    p_ip_v6_header->ip6_ctlun.ip6_un1.ip6_un1_plen = htons(payload_length);
}

void set_ipv6_hops_limit_by_user_input(struct ip6_hdr *p_ip_v6_header) {
    char input_buffer[USER_INPUT_FIELDS_BUFFER_SIZE];
    memset(input_buffer, '\0', USER_INPUT_FIELDS_BUFFER_SIZE);

    printf("Please enter maximum hops (default is 128)\n");
    fgets(input_buffer, USER_INPUT_FIELDS_BUFFER_SIZE, stdin);
    unsigned char hops_limit = 128;
    if (strlen(input_buffer) > 1) { hops_limit = (unsigned char) strtoul(input_buffer, NULL, 0); }

    p_ip_v6_header->ip6_ctlun.ip6_un1.ip6_un1_hlim = hops_limit;
}

/** Passed address is default for src and dst */
void set_ipv6_src_and_dst_addresses_by_user_input(struct ip6_hdr *p_ip_v6_header, struct in6_addr *p_default_address) {
    char input_buffer[USER_INPUT_FIELDS_BUFFER_SIZE];
    char string_address[INET6_ADDRSTRLEN];
    memset(input_buffer, '\0', USER_INPUT_FIELDS_BUFFER_SIZE);
    memset(string_address, '\0', INET6_ADDRSTRLEN);

    printf("Please enter source IPv6 address\n");
    fgets(input_buffer, USER_INPUT_FIELDS_BUFFER_SIZE, stdin);

    strncpy(string_address, input_buffer, strlen(input_buffer) - 1);

    p_ip_v6_header->ip6_src = *p_default_address;

    if (strlen(input_buffer) > 1 && inet_pton(AF_INET6, string_address, &(p_ip_v6_header->ip6_src)) != 1) {
        printf("Wrong address format, format should be xxxx::xxxx:xxxx:xxxx:xxxx, using default\n");
        p_ip_v6_header->ip6_src = *p_default_address;
    }

    memset(input_buffer, '\0', USER_INPUT_FIELDS_BUFFER_SIZE);
    memset(string_address, '\0', INET6_ADDRSTRLEN);

    printf("Please enter destination IPv6 address\n");
    fgets(input_buffer, USER_INPUT_FIELDS_BUFFER_SIZE, stdin);

    strncpy(string_address, input_buffer, strlen(input_buffer) - 1);

    p_ip_v6_header->ip6_dst = *p_default_address;

    if (strlen(input_buffer) > 1 && inet_pton(AF_INET6, string_address, &(p_ip_v6_header->ip6_dst)) != 1) {
        printf("Wrong address format, format should be xxxx::xxxx:xxxx:xxxx:xxxx, using default\n");
        p_ip_v6_header->ip6_dst = *p_default_address;
    }
}

void set_icmp_v6_type_and_code_by_user_input(struct icmp6_hdr *p_icmp_v6_header) {
    char input_buffer[USER_INPUT_FIELDS_BUFFER_SIZE];
    memset(input_buffer, '\0', USER_INPUT_FIELDS_BUFFER_SIZE);

    printf("Please enter ICMPv6 type (default is 128)\n");
    fgets(input_buffer, USER_INPUT_FIELDS_BUFFER_SIZE, stdin);
    unsigned char type = ICMP6_ECHO_REQUEST;
    if (strlen(input_buffer) > 1) { type = (unsigned char) strtoul(input_buffer, NULL, 0); }

    p_icmp_v6_header->icmp6_type = type;

    memset(input_buffer, '\0', USER_INPUT_FIELDS_BUFFER_SIZE);

    printf("Please enter ICMPv6 code (default is 0)\n");
    fgets(input_buffer, USER_INPUT_FIELDS_BUFFER_SIZE, stdin);
    unsigned char code = 0;
    if (strlen(input_buffer) > 1) { code = (unsigned char) strtoul(input_buffer, NULL, 0); }

    p_icmp_v6_header->icmp6_code = code;
}

void set_icmp_v6_message_body_by_user_input(struct icmp6_hdr *p_icmp_v6_header) {
    char input_buffer[USER_INPUT_FIELDS_BUFFER_SIZE];
    memset(input_buffer, '\0', USER_INPUT_FIELDS_BUFFER_SIZE);

    p_icmp_v6_header->icmp6_data32[0] = 0;

    switch (p_icmp_v6_header->icmp6_type){
        case ICMP6_ECHO_REQUEST:
        case ICMP6_ECHO_REPLY:
            printf("Please enter ICMPv6 ECHO id (default is random)\n");
            fgets(input_buffer, USER_INPUT_FIELDS_BUFFER_SIZE, stdin);
            p_icmp_v6_header->icmp6_id = htons((uint16_t) random());
            if (strlen(input_buffer) > 1) {
                p_icmp_v6_header->icmp6_id = htons((unsigned short) strtoul(input_buffer, NULL, 0));
            }

            memset(input_buffer, '\0', USER_INPUT_FIELDS_BUFFER_SIZE);
            printf("Please enter ICMPv6 ECHO seq number (default is random)\n");
            fgets(input_buffer, USER_INPUT_FIELDS_BUFFER_SIZE, stdin);
            p_icmp_v6_header->icmp6_seq = htons((uint16_t) random());
            if (strlen(input_buffer) > 1) {
                p_icmp_v6_header->icmp6_seq = htons((unsigned short) strtoul(input_buffer, NULL, 0));
            }

            break;

        case ICMP6_PACKET_TOO_BIG:
            printf("Please enter packet mtu (default is 0)\n");
            fgets(input_buffer, USER_INPUT_FIELDS_BUFFER_SIZE, stdin);
            if (strlen(input_buffer) > 1) {
                p_icmp_v6_header->icmp6_mtu = (unsigned int) strtoul(input_buffer, NULL, 0);
            }

            break;
        case ICMP6_PARAM_PROB:
            printf("Please enter packet pointer (default is 0)\n");
            fgets(input_buffer, USER_INPUT_FIELDS_BUFFER_SIZE, stdin);
            if (strlen(input_buffer) > 1) {
                p_icmp_v6_header->icmp6_pptr = (unsigned int) strtoul(input_buffer, NULL, 0);
            }

            break;

        default:
            printf("Please enter ICMP message body value (default is 0)\n");
            fgets(input_buffer, USER_INPUT_FIELDS_BUFFER_SIZE, stdin);
            if (strlen(input_buffer) > 1) {
                p_icmp_v6_header->icmp6_data32[0] = (unsigned int) strtoul(input_buffer, NULL, 0);
            }

            break;
    }

}

int create_icmp_v6_data_by_user_input(unsigned char **p_icmp_v6_data, unsigned short *p_icmp_v6_data_size){
    char input_buffer [65535 - sizeof(struct icmp6_hdr)];
    memset(input_buffer, '\0', sizeof(input_buffer));
    printf("Please enter ICMPv6 message:\n");
    fgets(input_buffer, sizeof(input_buffer), stdin);

    if (strlen(input_buffer) < 2){
        *p_icmp_v6_data = NULL;
        *p_icmp_v6_data_size = 0;
        printf("No message will be added.\n");
        return 0;
    }

    unsigned short size = (unsigned short) (strlen(input_buffer) - 1);

    unsigned char *p_trimmed_message = malloc(size);
    if (p_trimmed_message == NULL){
        printf("Cannot add message.\n");
        return -1;
    }

    memset(p_trimmed_message, '\0', size);
    memcpy(p_trimmed_message, input_buffer, size);

    *p_icmp_v6_data = p_trimmed_message;
    *p_icmp_v6_data_size = size;

    return 0;
}


int create_icmp_v6_packet(struct ip6_hdr *p_ip_v6_header,
                          struct icmp6_hdr *p_icmp_v6_header,
                          unsigned char *p_icmp_v6_data,
                          unsigned short icmp_v6_data_size,
                          unsigned char **p_out_ip_packet,
                          unsigned short *p_out_ip_packet_size) {

    unsigned short total_header_size = sizeof(struct ip6_hdr) + sizeof(struct icmp6_hdr);
    unsigned short full_ip_packet_size = total_header_size + icmp_v6_data_size;
    unsigned char *p_full_ip_packet = malloc(full_ip_packet_size);
    if (p_full_ip_packet == NULL){
        printf("Cannot create ICMPv6 packet.\n");
        return  -1;
    }

    //ipv6 automatic payload length
    if (p_ip_v6_header->ip6_ctlun.ip6_un1.ip6_un1_plen == 0){
        p_ip_v6_header->ip6_ctlun.ip6_un1.ip6_un1_plen = htons(sizeof(struct icmp6_hdr) + icmp_v6_data_size);
    }

    p_ip_v6_header->ip6_ctlun.ip6_un1.ip6_un1_nxt = ICMP_V6_PROTOCOL_NO;

    //concat packets
    memcpy(p_full_ip_packet, p_ip_v6_header, sizeof(struct ip6_hdr));
    memcpy(p_full_ip_packet + sizeof(struct ip6_hdr), p_icmp_v6_header, sizeof(struct icmp6_hdr));

    if (icmp_v6_data_size > 0){
        memcpy(p_full_ip_packet + sizeof(struct ip6_hdr) + sizeof(struct icmp6_hdr), p_icmp_v6_data, icmp_v6_data_size);
    }

    //calculate checksum
    if (p_icmp_v6_header->icmp6_cksum == 0){
        set_icmp_v6_checksum(p_ip_v6_header, p_full_ip_packet, icmp_v6_data_size);
    }

    //return full packet
    *p_out_ip_packet = p_full_ip_packet;
    *p_out_ip_packet_size = full_ip_packet_size;

    return 0;

}


int set_icmp_v6_checksum(struct ip6_hdr *p_ip_v6_header, unsigned char *p_full_ip_packet,
                         unsigned short icmp_v6_data_size) {

    struct icmp6_hdr *p_icmp_v6_header = (struct icmp6_hdr*) (p_full_ip_packet + sizeof(struct ip6_hdr));

    struct icmp_v6_pseudo_header_t icmp_v6_pseudo_header;
    icmp_v6_pseudo_header.ip_v6_dst = p_ip_v6_header->ip6_dst;
    icmp_v6_pseudo_header.ip_v6_src = p_ip_v6_header->ip6_src;
    icmp_v6_pseudo_header.icmp_v6_len = htonl(sizeof(struct icmp6_hdr) + icmp_v6_data_size);
    icmp_v6_pseudo_header.zeros[0] = 0;
    icmp_v6_pseudo_header.zeros[1] = 0;
    icmp_v6_pseudo_header.zeros[2] = 0;
    icmp_v6_pseudo_header.next_header = ICMP_V6_PROTOCOL_NO;

    unsigned int pseudo_sum = calculate_sum((unsigned short *) &icmp_v6_pseudo_header, sizeof(struct icmp_v6_pseudo_header_t));
    unsigned int icmp_v6_sum = calculate_sum((unsigned short *) p_icmp_v6_header, sizeof(struct icmp6_hdr) + icmp_v6_data_size);
    unsigned int total_sum = pseudo_sum + icmp_v6_sum;
    unsigned short checksum = (unsigned short) ~((total_sum & 0xffff) + (total_sum >> 16));


    p_icmp_v6_header->icmp6_cksum = checksum;

}

unsigned int calculate_sum(unsigned short *p_buffer, unsigned int size){
    register unsigned int sum = 0;
    while (size > 1) {
        sum += *(p_buffer++);
        size -= sizeof(unsigned short);
    }

    //add left byte, if there is any
    if (size) {
        sum += *(uint8_t *) p_buffer;
    }

    return sum;
}

int send_ethernet_packet(char *p_interface_name, unsigned char *p_ip_packet, unsigned int ip_packet_size){

    int socket_id = socket(PF_PACKET, SOCK_DGRAM, htons(ETH_P_ALL));
    if (socket_id < 0){
        printf("Socket cannot be opened, packet will not be sent\n");
        return -1;
    }

    struct sockaddr_ll socket_address;
    struct ether_header ethernet_header;
    if (fill_socket_and_ethernet_header_params(&socket_address, &ethernet_header, p_interface_name, socket_id) < 0){
        printf("Socket and ethernet frame parameters not added, packet will not be sent\n");
        return -1;
    }

    //create ethernet packet
    unsigned char *p_ethernet_packet = malloc(ip_packet_size + sizeof(struct ether_header));
    if (p_ethernet_packet == NULL){
        printf("Frame cannot be created, packet will not be sent\n");
        return -1;
    }

    memcpy(p_ethernet_packet, &ethernet_header, sizeof(struct ether_header));
    memcpy(p_ethernet_packet + sizeof(struct ether_header), p_ip_packet, ip_packet_size);

    for (int i = 0; i < ip_packet_size + sizeof(struct ether_header); i++) {
        if (i % 16 == 0)
            printf("\n");

        printf("%02x ", p_ethernet_packet[i]);
    }

    printf("\n");

    long sent_data_size = 0;
    sent_data_size = sendto(socket_id, p_ip_packet, ip_packet_size, 0, (struct sockaddr*) &socket_address,
                            sizeof(struct sockaddr_ll));
    if (sent_data_size < 0){
        printf("Packet sending error\n");
        return -1;
    }

    close(socket_id);
    return 0;

}

int fill_socket_and_ethernet_header_params(struct sockaddr_ll *p_socket_address,
                                           struct ether_header *p_ethernet_header,
                                           char *p_interface_name,
                                           int socket_id) {

    //index mapping
    struct ifreq interface_request_index;
    memset(&interface_request_index, 0, sizeof(struct ifreq));
    strncpy(interface_request_index.ifr_name, p_interface_name, strlen(p_interface_name));
    if (ioctl(socket_id, SIOCGIFINDEX, &interface_request_index) < 0){
        printf("Interface index mapping failed.\n");
        return -1;
    }

    //mac address mapping
    struct ifreq interface_request_mac;
    memset(&interface_request_mac, 0, sizeof(struct ifreq));
    strncpy(interface_request_mac.ifr_name, p_interface_name, strlen(p_interface_name));
    if (ioctl(socket_id, SIOCGIFHWADDR, &interface_request_mac) < 0){
        printf("Interface address mapping failed.\n");
        return -1;
    }

    p_socket_address->sll_family = AF_PACKET;
    p_socket_address->sll_protocol = htons(ETH_P_IPV6);
    p_socket_address->sll_ifindex = interface_request_index.ifr_ifindex;
    p_socket_address->sll_halen = ETH_ALEN;
    p_socket_address->sll_addr[0] = ((uint8_t *) &interface_request_mac.ifr_hwaddr.sa_data)[0];
    p_socket_address->sll_addr[1] = ((uint8_t *) &interface_request_mac.ifr_hwaddr.sa_data)[1];
    p_socket_address->sll_addr[2] = ((uint8_t *) &interface_request_mac.ifr_hwaddr.sa_data)[2];
    p_socket_address->sll_addr[3] = ((uint8_t *) &interface_request_mac.ifr_hwaddr.sa_data)[3];
    p_socket_address->sll_addr[4] = ((uint8_t *) &interface_request_mac.ifr_hwaddr.sa_data)[4];
    p_socket_address->sll_addr[5] = ((uint8_t *) &interface_request_mac.ifr_hwaddr.sa_data)[5];

    p_ethernet_header->ether_shost[0] = ((uint8_t *) &interface_request_mac.ifr_hwaddr.sa_data)[0];
    p_ethernet_header->ether_shost[1] = ((uint8_t *) &interface_request_mac.ifr_hwaddr.sa_data)[1];
    p_ethernet_header->ether_shost[2] = ((uint8_t *) &interface_request_mac.ifr_hwaddr.sa_data)[2];
    p_ethernet_header->ether_shost[3] = ((uint8_t *) &interface_request_mac.ifr_hwaddr.sa_data)[3];
    p_ethernet_header->ether_shost[4] = ((uint8_t *) &interface_request_mac.ifr_hwaddr.sa_data)[4];
    p_ethernet_header->ether_shost[5] = ((uint8_t *) &interface_request_mac.ifr_hwaddr.sa_data)[5];

    p_ethernet_header->ether_dhost[0] = ((uint8_t *) &interface_request_mac.ifr_hwaddr.sa_data)[0];
    p_ethernet_header->ether_dhost[1] = ((uint8_t *) &interface_request_mac.ifr_hwaddr.sa_data)[1];
    p_ethernet_header->ether_dhost[2] = ((uint8_t *) &interface_request_mac.ifr_hwaddr.sa_data)[2];
    p_ethernet_header->ether_dhost[3] = ((uint8_t *) &interface_request_mac.ifr_hwaddr.sa_data)[3];
    p_ethernet_header->ether_dhost[4] = ((uint8_t *) &interface_request_mac.ifr_hwaddr.sa_data)[4];
    p_ethernet_header->ether_dhost[5] = ((uint8_t *) &interface_request_mac.ifr_hwaddr.sa_data)[5];

    p_ethernet_header->ether_type = htons(ETH_P_IPV6);

    return 0;
}
